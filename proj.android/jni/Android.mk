LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

LOCAL_MODULE := cocos2dcpp_shared

LOCAL_MODULE_FILENAME := libcocos2dcpp

#LOCAL_SRC_FILES := hellocpp/main.cpp \
#                   ../../Classes/AppDelegate.cpp \
#                   ../../Classes/Game/Layers/BackgroundLayer	\
#                   ../../Classes/Game/Layers/GameScrollingLayer	\
#                   ../../Classes/Game/Layers/ShopPanel	\

#LOCAL_SRC_FILES := hellocpp/main.cpp \
#                   ../../Classes/AppDelegate.cpp \
#                   ../../Classes/HelloWorldScene.cpp \
#                  ../../Classes/Utils/GameUtils.cpp 

CPP_FILES_PATH := $(LOCAL_PATH)/../../Classes/
dirs := $(shell find $(CPP_FILES_PATH) -type d)

cppfilestemp1 := $(shell find $(CPP_FILES_PATH) -type d)
cppfilestemp2 := $(shell find $(cppfilestemp1) -name *.cpp)
cppfilestemp3 := $(sort $(cppfilestemp2))
cppfiles := $(subst $(LOCAL_PATH)/,,$(cppfilestemp3))

LOCAL_SRC_FILES := $(cppfiles)

LOCAL_SRC_FILES += hellocpp/main.cpp \
				   AdClasses/CPP_Bridge.cpp \
				   
#MM_CPP_FILES_PATH := $(LOCAL_PATH)/../../Classes/Generic/
#dirs := $(shell find $(MM_CPP_FILES_PATH) -type d)

#cppfilestemp1 := $(shell find $(MM_CPP_FILES_PATH) -type d)
#cppfilestemp2 := $(shell find $(cppfilestemp1) -name *.cpp)
#cppfilestemp3 := $(sort $(cppfilestemp2))
#mmcppfiles := $(subst $(LOCAL_PATH)/,,$(cppfilestemp3))
#LOCAL_SRC_FILES += $(mmcppfiles)
				
LOCAL_C_INCLUDES := $(LOCAL_PATH)/../../Classes
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../Classes/Generic/
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../Classes/Game/Layers/HudLayer
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../Classes/Game/Layers/BackgroundLayer
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../Classes/Game/Layers/GameScrollingLayer
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../Classes/Game/Layers/ShopPanel
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../Classes/Game/Layers/PauseLayer
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../Classes/Game/Scenes/GameOver
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../Classes/Game/Scenes/GameOverPre
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../Classes/Game/Scenes/GameScene
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../Classes/Game/Scenes/MainMenu
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../Classes/GameConstants
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../Classes/Generic/InAppHandler
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../Classes/SoundHandler
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../Classes/Util
LOCAL_C_INCLUDES += $(LOCAL_PATH)/InAppHandler
LOCAL_C_INCLUDES += $(LOCAL_PATH)/AdClasses

LOCAL_WHOLE_STATIC_LIBRARIES := cocos2dx_static
LOCAL_WHOLE_STATIC_LIBRARIES += cocosdenshion_static

# LOCAL_WHOLE_STATIC_LIBRARIES += box2d_static
LOCAL_WHOLE_STATIC_LIBRARIES += cocosbuilder_static
# LOCAL_WHOLE_STATIC_LIBRARIES += spine_static
LOCAL_WHOLE_STATIC_LIBRARIES += cocostudio_static
# LOCAL_WHOLE_STATIC_LIBRARIES += cocos_network_static
LOCAL_WHOLE_STATIC_LIBRARIES += cocos_extension_static

include $(BUILD_SHARED_LIBRARY)

$(call import-module,.)
$(call import-module,audio/android)

# $(call import-module,Box2D)
$(call import-module,editor-support/cocosbuilder)
# $(call import-module,editor-support/spine)
$(call import-module,editor-support/cocostudio)
# $(call import-module,network)
$(call import-module,extensions)
