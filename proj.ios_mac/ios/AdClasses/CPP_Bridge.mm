//
//  CPP_Bridge.cpp
//  SocialPuzzle
//
//  Created by Umair Javed on 20/03/2014.
//
//

#include "CPP_Bridge.h"
#include "AppController.h"
#import "MY_CPP_Bridge.h"
#include "MainMenu.h"
bool CPP_Bridge::isVideoAdAvailable(){
    
    
    if(CPP_Bridge::isVungleAdAvailable() || CPP_Bridge::isColonyAdAvailable()){
        return true;
    }
    return false;
}

//*******************************************************************************************
void CPP_Bridge::startVungle(){
    //[[GenericAdController sharedGenericAdController] startVungle];
}
bool CPP_Bridge::isVungleAdAvailable(){
  return false;//[[GenericAdController sharedGenericAdController] isVungleAdAvailable];
}
bool CPP_Bridge::showVungleAd(){
  return false;//[[GenericAdController sharedGenericAdController] showVungleVideoAd];
}
//*******************************************************************************************


//*******************************************************************************************
void CPP_Bridge::startAdColony(){
    //[[GenericAdController sharedGenericAdController] startAdColony];
}
bool CPP_Bridge::isColonyAdAvailable(){
  return false;//[[GenericAdController sharedGenericAdController] isColonyAdAvailable];
}
bool CPP_Bridge::showAdColonyVideoAd(int zone){
  return false;//[[GenericAdController sharedGenericAdController] showAdColonyVideoAd:zone];
}
//*******************************************************************************************
//*******************************************************************************************


void CPP_Bridge::startFlurryAd(){
    //[[GenericAdController sharedGenericAdController] startFlurryAds];
}
bool CPP_Bridge::isFlurryAdAvailable(){
  return false;//[[GenericAdController sharedGenericAdController] canShowFlurryVideo];
}
bool CPP_Bridge::showFlurryAd(){
  return false;//[[GenericAdController sharedGenericAdController] showFlurryVideo];
}

//*******************************************************************************************
//*******************************************************************************************


void CPP_Bridge::startAdsLibrary(){
    [[MY_CPP_Bridge sharedCPPBridge] initializeallnetworks];
//    [[GenericAdController sharedGenericAdController] startAdsLibrary];
    
}
void CPP_Bridge::showInterstitialAd(){
    [[MY_CPP_Bridge sharedCPPBridge] showChartBoostAd];
//    [[GenericAdController sharedGenericAdController] showInterstitialAd];
}

void CPP_Bridge::showMoreApps(){
    [[MY_CPP_Bridge sharedCPPBridge] showMoreApps];
//    [[GenericAdController sharedGenericAdController] showMoreAps];
   
}
void CPP_Bridge::showapplovinad(){
    [[MY_CPP_Bridge sharedCPPBridge] showapplovinad];
    //    [[GenericAdController sharedGenericAdController] showMoreAps];
    
}
void CPP_Bridge::showPlayHavenOnGameOver(){
    [[MY_CPP_Bridge sharedCPPBridge] showPlayHavenOnGameOver];
    //    [[GenericAdController sharedGenericAdController] showMoreAps];
    
}
void CPP_Bridge::showPlayHavenOnAppLaunch(){
    [[MY_CPP_Bridge sharedCPPBridge] showPlayHavenOnAppLaunch];
    //    [[GenericAdController sharedGenericAdController] showMoreAps];
    
}
void CPP_Bridge::showrevmobad(){
    [[MY_CPP_Bridge sharedCPPBridge] showFullScreenRevMobAd];
    //    [[GenericAdController sharedGenericAdController] showMoreAps];
    
}
void CPP_Bridge::startchartboost(){
    [[MY_CPP_Bridge sharedCPPBridge] initializeChartBoostNetwork];
    //    [[GenericAdController sharedGenericAdController] showMoreAps];
   
}
//*******************************************************************************************
//*******************************************************************************************


void CPP_Bridge::copenURL(std::string url){
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithUTF8String:url.c_str()]]];
}
//*******************************************************************************************


int CPP_Bridge::getScreenHeight(){

    return [UIScreen mainScreen].applicationFrame.size.height*[UIScreen mainScreen].scale;
}
void CPP_Bridge::authenticateLocalUser()
{
    //[[GCHelper sharedInstance] authenticateLocalUser];
}
void CPP_Bridge::highscore(){
    
    //    [[SNAdsManager sharedManager] giveMeFullScreenChartBoostAd];
    AppController *del=(AppController*)[UIApplication sharedApplication].delegate ;
    [del initGameCenter];
}
void CPP_Bridge::getDeviceNameAndVersion(){
    [[MY_CPP_Bridge sharedCPPBridge] getDeviceNameAndVersion];
}
void CPP_Bridge::rateMe(){
    [[MY_CPP_Bridge sharedCPPBridge] showReviewApp];
}
void CPP_Bridge::submitScore(){
    AppController *del=(AppController*)[UIApplication sharedApplication].delegate ;
    [del submitScore];
    
}

//***********************************************************************************************

void CPP_Bridge::cancelAdPlayHeaven(bool flag)
{
  [[MY_CPP_Bridge sharedCPPBridge] cancelAdPlayHeaven:flag];
}

void CPP_Bridge::onClosedRevMob()
{
  cocos2d::Director::getInstance()->startAnimation();
}

void CPP_Bridge::contentWillBeDisplayed()
{
    cocos2d::Director::getInstance()->stopAnimation();
}
void CPP_Bridge::contentDisplayDone()
{
    cocos2d::Director::getInstance()->startAnimation();
    auto scene = MainMenu::createScene();
    Director::getInstance()->runWithScene(scene);
}



