//
//  MY_CPP_Bridge.m
//  HangMangGame
//
//  Created by Zohaib Zahid on 27/08/2014.
//
//

#import "MY_CPP_Bridge.h"
  //#import "ALSdk.h"
  //#import "ALInterstitialAd.h"
#import "PlayHavenSDK.h"
#import "PHContentView.h"
  //#import "ALInterstitialAd.h"
#include "GameScene.h"

static NSUInteger gameOverCountForRating = 0;
#ifdef IS_IOS7_AND_UP
#define kRateURL @"itms-apps://itunes.apple.com/app/id914828967"
#else
#define kRateURL @"itms-apps://ax.itunes.apple.com/WebObjects/MZStore.woa/wa/viewContentsUserReviews?type=Purple+Software&id=914828967"
#endif
//@interface MY_CPP_Bridge() <ChartboostDelegate,PHContentViewDelegate,RevMobAdsDelegate>
//
//@end

@implementation MY_CPP_Bridge

static MY_CPP_Bridge *sharedManager = nil;

+ (MY_CPP_Bridge*)sharedCPPBridge
{
    if (sharedManager != nil)
    {
        return sharedManager;
    }
    
#if __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_4_0
    static dispatch_once_t safer;
    dispatch_once(&safer, ^(void)
                  {
                      sharedManager = [[MY_CPP_Bridge alloc] init];
                      
                  });
#else
    @synchronized([SNAdsManager class])
    {
        if (sharedManager == nil)
        {
            sharedManager = [[SNAdsManager alloc] init];
            
        }
    }
#endif
    return sharedManager;
}
-(void)initializeallnetworks
{
    //[self startapplovin];
    [self startRevMob];
    [self initializeChartBoostNetwork];
    [self startPlayhaven];
}
#pragma mark -
#pragma mark Revmob
-(void)startRevMob
{
    //[RevMobAds startSessionWithAppID:@"54225c0df3856f570718e424"];
    [RevMobAds startSessionWithAppID:@"54225c0df3856f570718e424" andDelegate:self];
    [RevMobAds session].connectionTimeout = 10;
    [RevMobAds load];
}
-(void)showFullScreenRevMobAd
{
    //[[RevMobAds session] showFullscreen];
    ad = [[RevMobAds session] fullscreen]; // you must retain this object
    ad.delegate = self;
    [ad showAd];

}

  //Muaaz
- (void)revmobAdDidReceive
{
    CPP_Bridge::contentWillBeDisplayed();
}
- (void)revmobUserClosedTheAd
{
  CPP_Bridge::onClosedRevMob();
}


#pragma mark -
#pragma mark Chartboost
-(void)initializeChartBoostNetwork
{
    
    NSString *chartboostID = @"54225b2589b0bb76d065b1a4";
    NSString *chartboostSig = @"02316c857806b4b05794452abc516e0b413afd3d";
    [Chartboost startWithAppId:chartboostID appSignature:chartboostSig delegate:sharedManager];
    [Chartboost cacheMoreApps:CBLocationGameScreen];
    [Chartboost cacheInterstitial:CBLocationGameScreen];
}

-(void)showChartBoostAd{
    [Chartboost showInterstitial:CBLocationGameScreen];
}

  //Muaaz
- (void)didDisplayInterstitial:(CBLocation)location{
    CPP_Bridge::contentWillBeDisplayed();
}
- (void)didDismissInterstitial:(NSString *)location{
    CPP_Bridge::contentDisplayDone();
}
- (void)didCloseInterstitial:(NSString *)location {
   CPP_Bridge::contentDisplayDone();
}


-(void)showMoreApps
{
    [Chartboost showMoreApps:CBLocationGameScreen];
}
#pragma mark -
#pragma mark Applovin
-(void)startapplovin
{
    //[ALSdk initializeSdk];

}
-(void)showapplovinad
{

    //[ALInterstitialAd showOver:[[UIApplication sharedApplication] keyWindow]];
}
#pragma mark -
#pragma mark Play Haven
-(void)startPlayhaven
{
    [[PHPublisherOpenRequest requestForApp:@"72cc9aad1be848abafe525164c584688" secret:@"dbbc4e2fd5bd4906aaaa4a1640597728" ] send];
  [[PHContentView alloc] setDelegate:sharedManager];
}
-(void)showPlayHavenOnAppLaunch
{
    request = [PHPublisherContentRequest requestForApp:@"72cc9aad1be848abafe525164c584688" secret:@"dbbc4e2fd5bd4906aaaa4a1640597728" placement:@"game_launch" delegate:sharedManager];
  [request send];
}
-(void)showPlayHavenOnGameOver
{
    
    request = [PHPublisherContentRequest requestForApp:@"72cc9aad1be848abafe525164c584688" secret:@"dbbc4e2fd5bd4906aaaa4a1640597728" placement:@"game_launch" delegate:sharedManager];
    [request send];
}

  //Muaaz
- (void)request:(PHPublisherContentRequest *)request contentWillDisplay:(PHContent *)content{
    CPP_Bridge::contentWillBeDisplayed();
}
- (void)request:(PHPublisherContentRequest *)request contentDidDismissWithType:(PHPublisherContentDismissType *)type{
    CPP_Bridge::contentDisplayDone();
}
//-(void)cancelAdPlayHeaven : (BOOL)flag
//{
//    //if(flag) [request cancel];
//}
//
//-(void)cancelAdRevMob
//{
//    //if (ad) [ad hideAd];
//}

-(void)getDeviceNameAndVersion
{
    NSString *device = [[UIDevice currentDevice]model ] ;
    NSLog(@"%@",device);
//    it gives you device name (iPhone, iPad)
    
    float version = [[[UIDevice currentDevice] systemVersion] floatValue];
    NSLog(@"%f",version);
}
#pragma mark -
#pragma mark Rate My App
- (BOOL)hasAppBeenRated{
    NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
    return [standardUserDefaults boolForKey:@"appHasBeenRated"];
}

- (void)setAppRated{
    NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
    [standardUserDefaults setBool:YES forKey:@"appHasBeenRated"];
    [standardUserDefaults synchronize];
}

- (void)showReviewApp{
    gameOverCountForRating++;
    if(gameOverCountForRating % 3 == 2)
    {
        if (![self hasAppBeenRated]) {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Please review this great app! " delegate:self cancelButtonTitle:@"NO" otherButtonTitles:@"YES", nil];
            alert.tag = 2;
            alert.delegate = self;
            [alert show];
            [alert release];
        }
    }
}
- (void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 1 && alertView.tag == 2)
    {// rate app
        [self rateApp];
    }
}

- (void)rateApp{
    [self setAppRated];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:kRateURL]];
}
@end
