//
//  MY_CPP_Bridge.h
//  HangMangGame
//
//  Created by Zohaib Zahid on 27/08/2014.
//
//
#import <Foundation/Foundation.h>
#import <Chartboost/Chartboost.h>
#import <RevMobAds/RevMobAds.h>
#import <UIKit/UIKit.h>
#import "PHContentView.h"
#import "PlayHavenSDK.h"
#import "CPP_Bridge.h"

  //#import "ALInterstitialAd.h"
@interface MY_CPP_Bridge : UIViewController  <ChartboostDelegate,RevMobAdsDelegate,UIApplicationDelegate,PHContentViewDelegate /*, ALAdLoadDelegate, ALAdDisplayDelegate*/>
{
  PHPublisherContentRequest *request;
  CPP_Bridge _delegate;
  RevMobFullscreen *ad;
}
//*************Chart Boost Functions*********************
-(void)initializeChartBoostNetwork;
-(void)showChartBoostAd;
-(void)showMoreApps;

//*************RevMob Functions*********************
-(void)startRevMob;
-(void)showFullScreenRevMobAd;
-(void)startapplovin;
-(void)showapplovinad;
-(void)initializeallnetworks;
-(void)startPlayhaven;
-(void)showPlayHavenOnAppLaunch;
-(void)showPlayHavenOnGameOver;
-(void)getDeviceNameAndVersion;
-(void)showReviewApp;
-(void)cancelAdPlayHeaven;
-(void)cancelAdRevMob;
- (void)revmobAdDidReceive;
- (void)revmobUserClosedTheAd;


  //- (void)contentViewDidDismiss:(PHContentView *)contentView;
  //- (void)contentViewDidLoad:(PHContentView *)contentView;
  //- (void)contentViewDidShow:(PHContentView *)contentView;

+ (MY_CPP_Bridge *)sharedCPPBridge;

@end