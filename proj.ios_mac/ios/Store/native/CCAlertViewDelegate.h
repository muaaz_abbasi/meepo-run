
#ifndef __CC_EXTENSION_CCALERTVIEW_DELEGATE_H_
#define __CC_EXTENSION_CCALERTVIEW_DELEGATE_H_



class CCAlertViewDelegate
{
public:
    virtual void alertViewClickedButtonAtIndex(int alerttag,int buttonIndex) = 0;
};


#endif // __CC_EXTENSION_CCALERTVIEW_DELEGATE_H_
