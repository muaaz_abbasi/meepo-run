//
//  InAppHandler.cpp
//  SocialPuzzle
//
//  Created by Umair Javed on 11/03/2014.
//
//

#include "InAppHandler.h"
#include "CCStore.h"
#include "CCStoreProduct.h"
#include "CCStorePaymentTransaction.h"
#include "CCNative.h"
#include "GameConstants.h"


using namespace std;
using namespace cocos2d;
bool InAppHandler::instanceFlag = false;
InAppHandler* InAppHandler::single = NULL;
InAppHandler::InAppHandler()
{
    
}

InAppHandler* InAppHandler::create()
{
    InAppHandler * pRet = new InAppHandler();
    
    if (pRet && pRet->init())
    {
        return pRet;
    }
    
    return NULL;
}
InAppHandler* InAppHandler::sharedHandler()
{
    if(! instanceFlag)
    {
        single = InAppHandler::create();
        instanceFlag = true;
        return single;
    }
    else
    {
        return single;
    }
}

bool InAppHandler::init()
{
    if (CCNode::init())
    {
        CCStore::sharedStore()->postInitWithTransactionObserver(this);

        return true;
    }
        return false;
}
/////*************************************************

void InAppHandler::loadProducts()
{
    CCArray* productsId = CCArray::create();

    productsId->addObject(CCString::create(MEEPO_LIVESA));
    productsId->addObject(CCString::create(MEEPO_LIVESB));
    productsId->addObject(CCString::create(MEEPO_LIVESC));
    productsId->addObject(CCString::create(MEEPO_LIVESD));
    productsId->addObject(CCString::create(MEEPO_LIVESE));
    productsId->addObject(CCString::create(MEEPO_LIVESF));
  
    CCStore::sharedStore()->loadProducts(productsId, this);
}

void InAppHandler::purchaseObjectWithID(const char* productId)
{
    if (CCStore::sharedStore()->purchase(productId))
    {
        CCNative::createAlert("Waiting", "Purchase IAP Product, please wait.", NULL,0);
        CCNative::showAlert();
    }
    else
    {
        this->loadProducts();
        CCNative::createAlert("ERROR", "Load products", "OK",0);
        CCNative::showAlert();
    }
}


void InAppHandler::restoreAll()
{
    if (CCStore::sharedStore()->restoreCompleted())
    {
    }
    else
    {
    }
}

//****************************************************

#pragma mark -
#pragma mark CCStoreTransactionObserver

void InAppHandler::transactionCompleted(CCStorePaymentTransaction* transaction)
{
    CCNative::cancelAlert();
    CCNative::createAlert("Purchase Completed", "Thank you.", "OK",0);
    CCNative::showAlert();
    
    printf("\n");
    printf("TransactionIdentifier: %s\n", transaction->getTransactionIdentifier().c_str());
    printf("\n");
    
    this->provideContentForID(transaction->getProductIdentifier().c_str());
  
    CCStore::sharedStore()->finishTransaction(transaction);
}

void InAppHandler::transactionFailed(CCStorePaymentTransaction* transaction)
{
    CCNative::cancelAlert();
    CCNative::createAlert("Purchase FAILED", transaction->getErrorDescription().c_str(), "OK",0);
    CCNative::showAlert();
    
    CCStore::sharedStore()->finishTransaction(transaction);
    
    Map<std::string,String*>infoMap;
    infoMap.insert("status",ccs("fail"));
    EventCustom event(IN_APP_PURCHASE);
    event.setUserData(&infoMap);
    Director::getInstance()->getEventDispatcher()->dispatchEvent(&event);
    
    
}

void InAppHandler::transactionRestored(CCStorePaymentTransaction* transaction)
{
    CCNative::cancelAlert();
    CCNative::createAlert("Purchase RESTORED", "Purchase restored.", "OK",0);
    CCNative::showAlert();
    
    printf("\n");
    printf("TransactionIdentifier: %s\n", transaction->getTransactionIdentifier().c_str());
    printf("OriginTransactionIdentifier: %s\n",
           transaction->getOriginalTransaction()->getTransactionIdentifier().c_str());
    printf("\n");
    
    
    this->provideContentForID(transaction->getProductIdentifier().c_str());
    
    CCStore::sharedStore()->finishTransaction(transaction);
}

#pragma mark -
#pragma mark CCStoreProductsRequestDelegate

void InAppHandler::requestProductsCompleted(CCArray* products, CCArray* invalidProductsId)
{
    
    
    return;
    
    CCNative::cancelAlert();
    CCNative::createAlert("LOAD PRODUCTS COMPLETED", "Load products completed. Check console output", "OK",0);
    CCNative::showAlert();
    
    printf("\n");
    for (int i = 0; i < products->count(); ++i)
    {
        CCStoreProduct* product = static_cast<CCStoreProduct*>(products->objectAtIndex(i));
        printf("PRODUCT ID: %s\n",              product->getProductIdentifier().c_str());
        printf("  localizedTitle: %s\n",        product->getLocalizedTitle().c_str());
        printf("  localizedDescription: %s\n",  product->getLocalizedDescription().c_str());
        printf("  priceLocale: %s\n",           product->getPriceLocale().c_str());
        printf("  price: %0.2f\n",              product->getPrice());
    }
    printf("\n");
    
    if (invalidProductsId && invalidProductsId->count() > 0)
    {
        printf("FOUND INVALID PRODUCTS ID\n");
        for (int i = 0; i < invalidProductsId->count(); ++i)
        {
            CCString* ccid = static_cast<CCString*>(invalidProductsId->objectAtIndex(i));
            printf("  %s\n", ccid->getCString());
        }
    }
    
    printf("\n");
}

void InAppHandler::requestProductsFailed(int errorCode, const char* errorString)
{
    CCNative::cancelAlert();
    CCNative::createAlert("ERROR", errorString, "OK",0);
    CCNative::showAlert();
}


void InAppHandler::provideContentForID(const char *identifier){
  
  bool removeAd = false; int contents = 0;
  if(strcmp(identifier,MEEPO_LIVESA)==0){
    contents = 10;
  }
  else if(strcmp(identifier,MEEPO_LIVESB)==0){
    removeAd=true;
    contents = 30;
  }
  else if(strcmp(identifier,MEEPO_LIVESC)==0){
    removeAd=true;
    contents = 70;
  }
  else if(strcmp(identifier,MEEPO_LIVESD)==0){
    removeAd=true;
    contents = 150;
  }
  else if(strcmp(identifier,MEEPO_LIVESE)==0){
    removeAd=true;
    contents = 450;
  }
  else if(strcmp(identifier,MEEPO_LIVESF)==0){
    removeAd=true;
    contents = 850;
  }

  int lives = UserDefault::getInstance()->getIntegerForKey(kPlayerLives);
  UserDefault::getInstance()->setIntegerForKey(kPlayerLives, lives+contents);
  UserDefault::getInstance()->flush();
  
  UserDefault::getInstance()->setBoolForKey(REMOVE_AD, removeAd);
  UserDefault::getInstance()->flush();
  
  

  Map<std::string,String*>infoMap;
  infoMap.insert("status",ccs("success"));
  infoMap.insert("identifier",ccs(identifier));

  EventCustom event(IN_APP_PURCHASE);
  event.setUserData(&infoMap);
  Director::getInstance()->getEventDispatcher()->dispatchEvent(&event);

}







