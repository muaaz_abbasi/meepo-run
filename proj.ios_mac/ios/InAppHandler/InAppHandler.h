//
//  InAppHandler.h
//  SocialPuzzle
//
//  Created by Umair Javed on 11/03/2014.
//
//

#ifndef __SocialPuzzle__InAppHandler__
#define __SocialPuzzle__InAppHandler__

#include <iostream>
#include "cocos2d.h"
#include "CCStoreTransactionObserver.h"
#include "CCStoreProductsRequestDelegate.h"
USING_NS_CC;


//#define BUY_MULTI "com.kpack.vers.multi"

class InAppHandler:public cocos2d::CCNode, public CCStoreTransactionObserver, public CCStoreProductsRequestDelegate
{
private:
    static bool instanceFlag;
    static InAppHandler *single;
    
    InAppHandler();
    virtual bool init();
    static InAppHandler * create(void);
    void provideContentForID(const char* productId);
public:
  
    
    
    static InAppHandler* sharedHandler();
    
    void loadProducts();
    void purchaseObjectWithID(const char* productId);
    void restoreAll();
    
#pragma mark -
#pragma mark CCStoreTransactionObserver
    
    virtual void transactionCompleted(CCStorePaymentTransaction* transaction);
    virtual void transactionFailed(CCStorePaymentTransaction* transaction);
    virtual void transactionRestored(CCStorePaymentTransaction* transaction);
    
#pragma mark -
#pragma mark CCStoreProductsRequestDelegate
    
    virtual void requestProductsCompleted(CCArray* products, CCArray* invalidProductsId = NULL);
    virtual void requestProductsFailed(int errorCode, const char* errorString);
    

    

    
};

#endif /* defined(__SocialPuzzle__InAppHandler__) */
