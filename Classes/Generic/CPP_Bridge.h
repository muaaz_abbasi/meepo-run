//
//  CPP_Bridge.h
//  SocialPuzzle
//
//  Created by Umair Javed on 20/03/2014.
//
//

#ifndef __SocialPuzzle__CPP_Bridge__
#define __SocialPuzzle__CPP_Bridge__

#include <iostream>
#include "cocos2d.h"

class CPP_Bridge
{
public:
    
    static void startVungle();
    static bool isVungleAdAvailable();
    static bool showVungleAd();
    
    static void startAdColony();
    static bool isColonyAdAvailable();
    static bool showAdColonyVideoAd(int zone);

    static void authenticateLocalUser();
    static void initGameCenter();
    static void highscore();
    static void rateMe();
    static void submitScore();
    
    static void startFlurryAd();
    static bool isFlurryAdAvailable();
    static bool showFlurryAd();
    
    static void startAdsLibrary();
    static void showInterstitialAd();
    static void showMoreApps();
    static void showapplovinad();
    static void showrevmobad();
    static void showplayhaven();
    static void startchartboost();
    static void showPlayHavenOnAppLaunch();
    static void showPlayHavenOnGameOver();
    static void copenURL(std::string url);
    
    
    static bool isVideoAdAvailable();
    static void getDeviceNameAndVersion();
    
    static int getScreenHeight();
  
    static void cancelAdPlayHeaven(bool flag);
    static void onClosedRevMob();
    static void contentWillBeDisplayed();
    static void contentDisplayDone();


};


#endif /* defined(__SocialPuzzle__CPP_Bridge__) */
