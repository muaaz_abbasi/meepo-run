/*
 * InAppManager.cpp
 *
 *  Created on: Aug 27, 2014
 *      Author: gaminations1
 */

#include "InAppManager.h"
//#include "InAppHandler.h"
#include "GameConstants.h"


using namespace std;
using namespace cocos2d;

bool InAppManager::instanceFlag = false;
InAppManager* InAppManager::single = NULL;
InAppManager::InAppManager()
{

}

InAppManager* InAppManager::create()
{
	InAppManager * pRet = new InAppManager();

    if (pRet && pRet->init())
    {



        return pRet;
    }

    return NULL;
}
InAppManager* InAppManager::sharedHandler()
{
    if(! instanceFlag)
    {
        single = InAppManager::create();
        instanceFlag = true;
        return single;
    }
    else
    {
        return single;
    }
}

bool InAppManager::init()
{
    if (CCNode::init())
    {

        return true;
    }
        return false;
}


void InAppManager::loadProducts()
{
  //InAppHandler::sharedHandler()->loadProducts();
}

void InAppManager::purchaseObjectWithID(const char* productId)
{
  //InAppHandler::sharedHandler()->purchaseObjectWithID(productId);
}


void InAppManager::restoreAll()
{
  //InAppHandler::sharedHandler()->restoreAll();
}




