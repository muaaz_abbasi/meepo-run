/*
 * InAppManager.h
 *
 *  Created on: Aug 27, 2014
 *      Author: gaminations1
 */

#ifndef INAPPMANAGER_H_
#define INAPPMANAGER_H_


#include "cocos2d.h"
using namespace cocos2d;

class InAppManager :public cocos2d::Node
{
private:
    static bool instanceFlag;
    static InAppManager *single;

    InAppManager();
    virtual bool init();
    static InAppManager * create(void);
    void provideContentForID(const char* productId);
public:

    static InAppManager* sharedHandler();

    void loadProducts();
    void purchaseObjectWithID(const char* productId);
    void restoreAll();

};

#endif /* INAPPMANAGER_H_ */
