#include "AppDelegate.h"
#include "MainMenu.h"
#include "GameConstants.h"
#include "SoundHandler.h"
//#include "GameScene.h"
//#include "GameOver.h"
#include "CPP_Bridge.h"
#include "InAppManager.h"
USING_NS_CC;

AppDelegate::AppDelegate() {

}

AppDelegate::~AppDelegate() 
{
}

bool AppDelegate::applicationDidFinishLaunching() {
  
  // initialize director
  auto director = Director::getInstance();
  auto glview = director->getOpenGLView();
  if(!glview) {
    glview = GLView::create("My Game");
    director->setOpenGLView(glview);
  }
  
  Size frameSize = glview->getFrameSize();
  cocos2d::log("applicationDidFinishLaunching1");
  std::vector<std::string> searchPath;
  
  ResolutionPolicy policy =ResolutionPolicy::FIXED_WIDTH;
  if(frameSize.height == 1024 || frameSize.height == 2048){
    policy = ResolutionPolicy::FIXED_HEIGHT;
  }
  cocos2d::log("applicationDidFinishLaunching2");
  auto designResolutionSize = TARGET_DESIGN_RESOLUTION_SIZE;
  glview->setDesignResolutionSize(designResolutionSize.width, designResolutionSize.height, policy);
  
  if (frameSize.height >= resource3.size.height)
  {
    searchPath.push_back(resource3.directory);
    CCLOG("resource3.directory is %s ", resource3.directory);
    float scaleValue = MIN(resource3.size.height/designResolutionSize.height, resource3.size.width/designResolutionSize.width);
    director->setContentScaleFactor(scaleValue);
  }
  else if (frameSize.height >= resource2.size.height)
  {
    searchPath.push_back(resource2.directory);
    CCLOG("resource2.directory is %s ", resource2.directory);
    float scaleValue = MIN(resource2.size.height/designResolutionSize.height, resource2.size.width/designResolutionSize.width);
    director->setContentScaleFactor(scaleValue);
  }
  else
  {
    searchPath.push_back(resource1.directory);
    CCLOG("resource1.directory is %s ", resource1.directory);
    director->setContentScaleFactor(MIN(resource1.size.height/designResolutionSize.height, resource1.size.width/designResolutionSize.width));
  }

  searchPath.push_back("CCBRes");
  FileUtils::getInstance()->setSearchPaths(searchPath);

  SoundHandler::getInstance()->initiate();

  InAppManager::sharedHandler()->loadProducts();
  
  // turn on display FPS
  director->setDisplayStats(false);

  // set FPS. the default value is 1.0/60 if you don't call this
  director->setAnimationInterval(1.0 / 60);

  if(!UserDefault::getInstance()->getBoolForKey(REMOVE_AD))
  {
    CPP_Bridge::startAdsLibrary();
    CPP_Bridge::showPlayHavenOnAppLaunch();
  }
  else
  {
    auto scene = MainMenu::createScene();
    director->runWithScene(scene);
  }
  
  return true;
}

// This function will be called when the app is inactive. When comes a phone call,it's be invoked too
void AppDelegate::applicationDidEnterBackground() {
  Director::getInstance()->stopAnimation();

  // if you use SimpleAudioEngine, it must be pause
  CocosDenshion::SimpleAudioEngine::getInstance()->pauseBackgroundMusic();
}

// this function will be called when the app is active again
void AppDelegate::applicationWillEnterForeground() {
  Director::getInstance()->startAnimation();

  // if you use SimpleAudioEngine, it must resume here
  CocosDenshion::SimpleAudioEngine::getInstance()->resumeBackgroundMusic();
}
