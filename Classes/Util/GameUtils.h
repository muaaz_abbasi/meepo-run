#ifndef __GAMEUTILS_H__
#define __GAMEUTILS_H__

#include "cocos2d.h"
using namespace cocos2d;
using namespace std;

class GameUtils
{
public:
  static string formatedTime(int seconds);
  static float getCustomOffset(int layers);
  static void adjustScreen(Node *node);
  static void renewLbl1(Node *node);
  static void renewLbl2(Node *node);
  static int getUnique(int min,int max);
};

#endif // __GAMEUTILS_H__
