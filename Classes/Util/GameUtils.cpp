#include "GameUtils.h"
#include "GameConstants.h"

USING_NS_CC;

string GameUtils::formatedTime(int seconds) {
  int c = floor(seconds / 100);
  int s = seconds % 100;

  ostringstream str;
  str << (c < 10 ? "0" : "")<< c << " . " <<(s < 10 ? "0" : "") << s <<"\"";
  
  return str.str();
}

float GameUtils::getCustomOffset(int layers)
{
  auto plistDict = Dictionary::createWithContentsOfFile(kGameConfig);
  auto gameData = (__Dictionary*)plistDict->objectForKey("GameData");
  auto gameModes = (__Dictionary*)gameData->objectForKey("GameLayersOffsets");
  float offset=gameModes->valueForKey(String::createWithFormat("%d",layers)->getCString())->intValue();
  return offset;
}
void GameUtils::adjustScreen(Node *node)
{
  Size winSize = Director::getInstance()->getWinSize();
  Size designResolutionSize = TARGET_DESIGN_RESOLUTION_SIZE;
    
  node->setPosition(Point((winSize.width-designResolutionSize.width)/2, (winSize.height-designResolutionSize.height)/2));
    
}

void GameUtils::renewLbl1(Node *node)
{
  auto oldLbl = node->getChildByTag(kTagCurrntTimeLbl);
  Point p= oldLbl->getPosition();
  oldLbl->removeFromParentAndCleanup(true);
  auto lblNew = Label::createWithTTF("", GameFonts, 60);
  lblNew->setPosition(p);
  lblNew->setTag(kTagCurrntTimeLbl);
  node->addChild(lblNew,10);
}

void GameUtils::renewLbl2(Node *node)
{
  auto oldLbl2 = node->getChildByTag(kTagBestTimeLbl);
  Point p2= oldLbl2->getPosition();
  oldLbl2->removeFromParentAndCleanup(true);
  auto lblNew2 = Label::createWithTTF("", GameFonts, 60);
  lblNew2->setPosition(p2);
  lblNew2->setTag(kTagBestTimeLbl);
  node->addChild(lblNew2,20);
}

int GameUtils::getUnique(int min,int max)
{
  bool flag = true;
  int frequency = min+arc4random()%max;
  while(flag){
    if(frequency%2==0){
      frequency = min+arc4random()%max;
    }
    else flag = false;
  }
  return frequency;
}
