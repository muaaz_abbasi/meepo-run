#ifndef __GNMAINMENU_SCENE_H__
#define __GNMAINMENU_SCENE_H__

#include "cocos2d.h"
#include "extensions/cocos-ext.h"
#include "cocosbuilder/CocosBuilder.h"
using namespace cocos2d;
using namespace cocosbuilder;

class MainMenu : public Layer, public CCBMemberVariableAssigner, public CCBSelectorResolver
{
public:
  static cocos2d::Scene* createScene();
  void soundToggleBtnAction();
  void musicToggleBtnAction();
  void moreGamesBtnAction();
  void updatePreferences();
  CREATE_FUNC(MainMenu);
  virtual bool init();
  virtual SEL_MenuHandler onResolveCCBCCMenuItemSelector(Ref * pTarget, const char * pSelectorName);
  virtual extension::Control::Handler onResolveCCBCCControlSelector(Ref * pTarget, const char * pSelectorName);
  virtual bool onAssignCCBMemberVariable(Ref * pTarget, const char * pMemberVariableName, Node * node);
  void onMenuItemPressed(Ref *obj);
  
private:
  Node* _node;
};

#endif // __GNMAINMENU_SCENE_H__
