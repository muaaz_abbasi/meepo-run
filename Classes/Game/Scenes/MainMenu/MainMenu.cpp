#include "MainMenu.h"
#include "GameConstants.h"
#include "GameUtils.h"
#include "SoundHandler.h"
#include "GameScene.h"
//#include "CPP_Bridge.h"

USING_NS_CC;

Scene* MainMenu::createScene()
{
  auto scene = Scene::create();
  auto layer = MainMenu::create();
  scene->addChild(layer);
  return scene;
}

bool MainMenu::init()
{
  if ( !Layer::init() )
  {
      return false;
  }
  
  NodeLoaderLibrary* ccNodeLoaderLibrary = NodeLoaderLibrary::newDefaultNodeLoaderLibrary();
  CCBReader* ccbReader = new CCBReader(ccNodeLoaderLibrary);
  ccbReader->autorelease();

  _node = ccbReader->readNodeGraphFromFile("MainMenu.ccbi", this, TARGET_DESIGN_RESOLUTION_SIZE);
  if(_node)
    this->addChild(_node);

  GameUtils::adjustScreen(_node);
  updatePreferences();
  
  return true;
}

void MainMenu::updatePreferences()
{
  Menu* menu = (Menu*)_node->getChildByTag(kTagMenu);
  MenuItemImage* soundItem = (MenuItemImage*)menu->getChildByTag(kTagSoundItem);
  MenuItemImage* musicItem = (MenuItemImage*)menu->getChildByTag(kTagMusicItem);
  
  Sprite* soundOff = Sprite::createWithSpriteFrameName("Sound_button_Off.png");
  Sprite* soundOn = Sprite::createWithSpriteFrameName("Sound_button.png");
  Sprite* musicOff = Sprite::createWithSpriteFrameName("Music_button_Off.png");
  Sprite* musicOn = Sprite::createWithSpriteFrameName("Music_button.png");
  
  if(UserDefault::getInstance()->getBoolForKey(kSoundEnabled)==false)
  {
    soundItem->setNormalImage(soundOff);
  }
  else
  {
    soundItem->setNormalImage(soundOn);
  }
  if(UserDefault::getInstance()->getBoolForKey(kMusicEnabled)==false)
  {
      musicItem->setNormalImage(musicOff);
  }
  else
  {
      musicItem->setNormalImage(musicOn);
  }
 
}

void MainMenu:: soundToggleBtnAction()
{

  if (UserDefault::getInstance()->getBoolForKey(kSoundEnabled)==false)
  {
    SoundHandler::getInstance()->gameSoundON();
    UserDefault::getInstance()->setBoolForKey(kSoundEnabled, true);
    UserDefault::getInstance()->flush();
  }
  else
  {
    SoundHandler::getInstance()->gameSoundOFF();
    UserDefault::getInstance()->setBoolForKey(kSoundEnabled, false);
    UserDefault::getInstance()->flush();
  }
  
  updatePreferences();
}

void MainMenu:: musicToggleBtnAction()
{

  if (UserDefault::getInstance()->getBoolForKey(kMusicEnabled)==false)
  {
    SoundHandler::getInstance()->gameBackGroundMusicON();
    UserDefault::getInstance()->setBoolForKey(kMusicEnabled, true);
    UserDefault::getInstance()->flush();
  }
  else
  {
    SoundHandler::getInstance()->gameBackGroundMusicOFF();
    UserDefault::getInstance()->setBoolForKey(kMusicEnabled, false);
    UserDefault::getInstance()->flush();
  }
  updatePreferences();
}

SEL_MenuHandler MainMenu::onResolveCCBCCMenuItemSelector(Ref * pTarget, const char * pSelectorName)
{
    CCB_SELECTORRESOLVER_CCMENUITEM_GLUE(this, pSelectorName, MainMenu::onMenuItemPressed);
    return NULL;
}

extension::Control::Handler MainMenu::onResolveCCBCCControlSelector(Ref * pTarget, const char * pSelectorName)
{
    return NULL;
}
bool MainMenu::onAssignCCBMemberVariable(Ref * pTarget, const char * pMemberVariableName, Node * node)
{
    return false;
}

void MainMenu::onMenuItemPressed(Ref *obj)
{
  MenuItem *menuItem = (MenuItem *) obj;
  int tag = menuItem->getTag();
 
  if(tag >= kTagNormalMode && tag <= kTagInsaneMode)
  {

    ValueMap plistMap = FileUtils::getInstance()->getValueMapFromFile(kGameConfig);
    ValueMap gameData = plistMap.at("GameData").asValueMap();
    ValueMap gameModes = gameData.at("GameModes").asValueMap();
    int layers=gameModes.at(String::createWithFormat("%d",tag)->getCString()).asInt();
    auto scene = GameScene::createSceneWithLayers(layers,true);
    Director::getInstance()->replaceScene(scene);
    
  }
  else if (tag == kTagMoreGames)
  {
    moreGamesBtnAction();
  }
  else if (tag >= kTagMusicToggle && tag <= kTagRate)
  {
    switch (tag)
    {
      case kTagMusicToggle:
        musicToggleBtnAction();
        break;
      case kTagSoundToggle:
        soundToggleBtnAction();
        break;
      case kTagRate:
        //CPP_Bridge::rateMe();
        break;
    }
  }
}

void MainMenu::moreGamesBtnAction()
{
  //CPP_Bridge::showMoreApps();
}
