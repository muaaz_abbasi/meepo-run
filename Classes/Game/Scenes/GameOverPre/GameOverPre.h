#ifndef NOBDYDIESALN_GameOverPre_H__
#define NOBDYDIESALN_GameOverPre_H__

#include "cocos2d.h"
#include "extensions/cocos-ext.h"
#include "cocosbuilder/CocosBuilder.h"
using namespace cocos2d;
using namespace cocosbuilder;
using namespace std;

class GameOverPre : public Layer, public CCBMemberVariableAssigner, public CCBSelectorResolver
{
public:
  bool init();
  static GameOverPre* create();
  static Scene* createScene();
  virtual SEL_MenuHandler onResolveCCBCCMenuItemSelector(Ref * pTarget, const char * pSelectorName);
  virtual extension::Control::Handler onResolveCCBCCControlSelector(Ref * pTarget, const char * pSelectorName);
  virtual bool onAssignCCBMemberVariable(Ref * pTarget, const char * pMemberVariableName, Node * node);
  void setPlayerLifeAndBest();
  void updateLives();
  void consumeLivesToContinue();
  void OnMenuItemPressed(Ref *obj);
  void onCloseShop();
  virtual void onExit();
  virtual void onEnter();
private:
  int _lives;
  int _reqLives;
  Node* _node;
  Label* _lblCurrent;
  Label* _lblRequired;
};

#endif // NOBDYDIESALN_GameOverPre_H__
