#include "GameOverPre.h"
#include "GameConstants.h"
#include "GameOver.h"
#include "GameScene.h"
#include "GameUtils.h"
#include "Shop.h"

USING_NS_CC;
void GameOverPre::onEnter()
{
  Layer::onEnter();
}
Scene* GameOverPre::createScene()
{
  auto scene = Scene::create();
  auto layer = GameOverPre::create();
  scene->addChild(layer);
  return scene;
}

GameOverPre* GameOverPre::create()
{
  GameOverPre *pLayer = new GameOverPre();
  if (pLayer && pLayer->init())
  {
      pLayer->autorelease();
      return pLayer;
  }
  CC_SAFE_DELETE(pLayer);
  return NULL;
}

// on "init" you need to initialize your instance
bool GameOverPre::init()
{
  if ( !Layer::init() )
  {
    return false;
  }
  
  Size _winSize = TARGET_DESIGN_RESOLUTION_SIZE;
  
  NodeLoaderLibrary* ccNodeLoaderLibrary = NodeLoaderLibrary::newDefaultNodeLoaderLibrary();
  CCBReader* ccbReader = new CCBReader(ccNodeLoaderLibrary);
  ccbReader->autorelease();
    
  _node = ccbReader->readNodeGraphFromFile("GameOverPre.ccbi", this, TARGET_DESIGN_RESOLUTION_SIZE);
  GameUtils::adjustScreen(_node);
  if(_node)
    this->addChild(_node);

  setPlayerLifeAndBest();
  return true;
}

SEL_MenuHandler GameOverPre::onResolveCCBCCMenuItemSelector(Ref * pTarget, const char * pSelectorName)
{
  CCB_SELECTORRESOLVER_CCMENUITEM_GLUE(this, pSelectorName, GameOverPre::OnMenuItemPressed);
  return NULL;
}

extension::Control::Handler GameOverPre::onResolveCCBCCControlSelector(Ref * pTarget, const char * pSelectorName)
{
  return NULL;
}
bool GameOverPre::onAssignCCBMemberVariable(Ref * pTarget, const char * pMemberVariableName, Node * node)
{
  return false;
}

void GameOverPre::OnMenuItemPressed(Ref *obj)
{
  MenuItem *menuItem = (MenuItem *) obj;
  int tag = menuItem->getTag();
  
  if(kTagContinue == tag)
  {
    consumeLivesToContinue();
  }
  else if(kTagSkip == tag)
  {
    auto scene = GameOver::createScene();
    Director::getInstance()->replaceScene(scene);
  }
}

void GameOverPre::setPlayerLifeAndBest()
{
  if (UserDefault::getInstance()->getBoolForKey(kNotSetFirst) == false)
  {
    int layers = UserDefault::getInstance()->getIntegerForKey("num_layers");
    
    for (int i=1 ; i<=layers; i++) {
      UserDefault::getInstance()->setIntegerForKey(String::createWithFormat("%s%d",kTagGameTimeLast,layers)->getCString(), 0);
      UserDefault::getInstance()->flush();
    }
   
    UserDefault::getInstance()->setBoolForKey(kNotSetFirst, true);
    UserDefault::getInstance()->flush();
    UserDefault::getInstance()->setIntegerForKey(kPlayerLives, kPlayerLivesMax);
    UserDefault::getInstance()->flush();
    UserDefault::getInstance()->setIntegerForKey(kPlayerLivesReq, kPlayerLivesReqMax);
    UserDefault::getInstance()->flush();
  }
  updateLives();
}

void GameOverPre::updateLives()
{
  _lives = UserDefault::getInstance()->getIntegerForKey(kPlayerLives);
  _reqLives = UserDefault::getInstance()->getIntegerForKey(kPlayerLivesReq);
 
  Label* livesCurrentLbl = (Label*)_node->getChildByTag(kTagLivesCurrentLbl);
  livesCurrentLbl->setString(String::createWithFormat("%d",_lives)->getCString());
  Label* livesCurrentReq = (Label*)_node->getChildByTag(kTagLivesReqLbl);
  livesCurrentReq->setString(String::createWithFormat("%d",_reqLives)->getCString());
}

void GameOverPre::consumeLivesToContinue()
{
  if(_lives >= _reqLives)
  {
    int livesLeft = _lives - _reqLives;
    UserDefault::getInstance()->setIntegerForKey(kPlayerLives, livesLeft);
    UserDefault::getInstance()->flush();

    if(_reqLives>1)
    {
      UserDefault::getInstance()->setIntegerForKey(kPlayerLivesReq, --_reqLives);
      UserDefault::getInstance()->flush();
    }
    
    int layers = UserDefault::getInstance()->getIntegerForKey("num_layers");
    Director::getInstance()->replaceScene((Scene*)GameScene::createWithLayers(layers,false));
    
  }
  else
  {
    Shop *shop = (Shop*)Shop::create(_lives);
    shop->setTag(kTagShopContainer);
    addChild(shop);
  }
}

void GameOverPre::onCloseShop()
{
  updateLives();
}

void GameOverPre::onExit()
{
  Layer::onExit();
}

