#include "GameOver.h"
#include "GameUtils.h"
#include "GameConstants.h"
#include "GameScene.h"
#include "MainMenu.h"
//#include "CPP_Bridge.h"

USING_NS_CC;

Scene* GameOver::createScene()
{
  auto scene = Scene::create();
  auto layer = GameOver::create();
  scene->addChild(layer);
  return scene;
}

GameOver* GameOver::create()
{
  GameOver *pLayer = new GameOver();
  if (pLayer && pLayer->init())
  {
    pLayer->autorelease();
    return pLayer;
  }
  CC_SAFE_DELETE(pLayer);
  return NULL;
}

bool GameOver::init()
{
  if ( !Layer::init() )
  {
      return false;
  }
  
  //if(!UserDefault::getInstance()->getBoolForKey(REMOVE_AD))
    //CPP_Bridge::showrevmobad();
  
  NodeLoaderLibrary* ccNodeLoaderLibrary = NodeLoaderLibrary::newDefaultNodeLoaderLibrary();
  CCBReader* ccbReader = new CCBReader(ccNodeLoaderLibrary);
  ccbReader->autorelease();
  
  _node = ccbReader->readNodeGraphFromFile("GameOverPost.ccbi", this, TARGET_DESIGN_RESOLUTION_SIZE);
  GameUtils::adjustScreen(_node);
  if(_node)
    this->addChild(_node);
  
  log("GameOverPost added node++++");

  int time = UserDefault::getInstance()->getIntegerForKey(kTagGameTime);
  updateBestTime(time);

  log("GameOverPost updated best time++++");

  //GameUtils::renewLbl1(_node);
  auto timeCurrentLbl = (Label*)_node->getChildByTag(kTagCurrntTimeLbl);
  timeCurrentLbl->setColor(Color3B(0, 0, 0));
  timeCurrentLbl->setString(GameUtils::formatedTime(time));
        
  return true;
}

void GameOver::updateBestTime(int time)
{
  //GameUtils::renewLbl2(_node);

  log("GameOverPost updated best time++ renew label ++++");
  auto timeBestLbl = (Label*)_node->getChildByTag(kTagBestTimeLbl);
  timeBestLbl->setColor(Color3B(0, 0, 0));
  
  int layers = UserDefault::getInstance()->getIntegerForKey(kTagGameNumLayers);
  
  int bestLast = UserDefault::getInstance()->getIntegerForKey(String::createWithFormat("%s%d",kTagGameTimeLast,layers)->getCString());
  timeBestLbl->setString(GameUtils::formatedTime(bestLast));
  
  if(time>bestLast)
  {
    timeBestLbl->setString(GameUtils::formatedTime(time));
    UserDefault::getInstance()->setIntegerForKey(String::createWithFormat("%s%d",kTagGameTimeLast,layers)->getCString(), time);
    UserDefault::getInstance()->flush();
  }
}

SEL_MenuHandler GameOver::onResolveCCBCCMenuItemSelector(Ref * pTarget, const char * pSelectorName)
{
  CCB_SELECTORRESOLVER_CCMENUITEM_GLUE(this, pSelectorName, GameOver::OnMenuItemPressed);
  return NULL;
}

extension::Control::Handler GameOver::onResolveCCBCCControlSelector(Ref * pTarget, const char * pSelectorName)
{
  return NULL;
}
bool GameOver::onAssignCCBMemberVariable(Ref * pTarget, const char * pMemberVariableName, Node * node)
{
  return false;
}

void GameOver::OnMenuItemPressed(Ref *obj)
{
  MenuItem *menuItem = (MenuItem *) obj;
  int tag = menuItem->getTag();
  
  if(tag == kTagBackItem)
  {
    auto scene = MainMenu::createScene();
    Director::getInstance()->replaceScene(scene);
  }
  else if(tag == kTagRestartItem)
  {
    int layers = UserDefault::getInstance()->getIntegerForKey(kTagGameNumLayers);
    Director::getInstance()->replaceScene((Scene*)GameScene::createWithLayers(layers,true));
  }
}


