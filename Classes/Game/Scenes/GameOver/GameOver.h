#ifndef __GAMEOVER_SCENE_H__
#define __GAMEOVER_SCENE_H__

#include "cocos2d.h"
#include "extensions/cocos-ext.h"
#include "cocosbuilder/CocosBuilder.h"
#include "BackgroudLayer.h"

using namespace cocos2d;
using namespace cocosbuilder;

class GameOver : public cocos2d::Layer, public CCBMemberVariableAssigner, public CCBSelectorResolver
{
public:
  static cocos2d::Scene* createScene();
  static GameOver* create();
  bool init();
  void updateBestTime(int time);
  void OnMenuItemPressed(Ref *obj);
  SEL_MenuHandler resolveSelectors(Ref * pTarget,const char * pSelectorName);
  virtual SEL_MenuHandler onResolveCCBCCMenuItemSelector(Ref * pTarget, const char * pSelectorName);
  virtual extension::Control::Handler onResolveCCBCCControlSelector(Ref * pTarget, const char * pSelectorName);
  virtual bool onAssignCCBMemberVariable(Ref * pTarget, const char * pMemberVariableName, Node * node);
private:
  Node* _node;
};

#endif // __GAMEOVER_SCENE_H__
