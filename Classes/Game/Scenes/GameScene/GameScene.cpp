#include "GameScene.h"
#include "GameConstants.h"
#include "GameUtils.h"
//#include "PauseLayer.h"
#include "HudLayer.h"

GameScene* GameScene::instance() {
  static GameScene *scene;
  if(!scene)
  {
    scene = new GameScene();
  }
  return scene;
}


Scene* GameScene::createSceneWithLayers(int pLayers,bool isFirst)
{
  auto scene = Scene::create();
  auto parentlayer = GameScene::createWithLayers(pLayers,isFirst);
  scene->addChild(parentlayer);
  return scene;
}

GameScene* GameScene::createWithLayers(int pLayers,bool isFirst)
{
  GameScene *pLayer = new GameScene();
  if (pLayer && pLayer->initWithLayers(pLayers,isFirst))
  {
      pLayer->autorelease();
      return pLayer;
  }
  CC_SAFE_DELETE(pLayer);
  return NULL;
}

bool GameScene::initWithLayers(int pLayers,bool isFirst)
{
  if ( !Layer::init() )
  {
      return false;
  }
  
  _numLayers = pLayers;
  _customOffset = GameUtils::getCustomOffset(pLayers);
  NodeLoaderLibrary* ccNodeLoaderLibrary = NodeLoaderLibrary::newDefaultNodeLoaderLibrary();
  CCBReader* ccbReader = new CCBReader(ccNodeLoaderLibrary);
  ccbReader->autorelease();
  
  _node = ccbReader->readNodeGraphFromFile("GamePlay.ccbi", this, TARGET_DESIGN_RESOLUTION_SIZE);
  if(_node)
      this->addChild(_node);
  
  //addHud();
  auto bg = _node->getChildByTag(kTagGameSceneBG);
  bg->setAnchorPoint(Point::ZERO);
  bg->setPosition(Point::ZERO);
  
  if(isFirst)
  {
    firstRunActions(pLayers);
  }
  else
    m_time = UserDefault::getInstance()->getIntegerForKey(kTagGameTime);
  
  initBackgroundLayer();
  //scheduleUpdate();

  return true;
}

void GameScene::addHud()
{

  HudLayer* hudlayer = HudLayer::create();
  _node->addChild(hudlayer);
  Node* hud = hudlayer->getChildByTag(1010);
  auto bgrd = hud->getChildByTag(kTagHudBg);
  Size size = hud->getChildByTag(kTagHudBg)->getContentSize();
  
  Size frame = Director::getInstance()->getOpenGLView()->getFrameSize();
  if(frame.width != 640)
    hud->setPositionX(80);
  
  _timer_label = Label::createWithTTF("", GameFonts, 40);
  _timer_label->setColor(Color3B(0, 0, 0));
  _timer_label->setAnchorPoint(Point(0.5, 0.5));
  _timer_label->setPosition(Point(size.width/2+10, size.height/2+30));
  _timer_label->setTag(kTagTimerLabel);
  _timer_label->setString(GameUtils::formatedTime(m_time));
  bgrd->addChild(_timer_label,10);
  
}

void GameScene::firstRunActions(int pLayer)
{
  m_time = 0;
  UserDefault::getInstance()->setIntegerForKey(kTagGameNumLayers, pLayer);
  UserDefault::getInstance()->flush();
}

void GameScene::initBackgroundLayer()
{
  auto bgSprite = (Sprite*)_node->getChildByTag(kTagGameSceneBG);
  float containerH = bgSprite->getContentSize().height-176;
  float containerW = bgSprite->getContentSize().width;
  float unitH = (containerH / _numLayers);

  float currentPosY = _customOffset;
  
  for (int i=0; i<_numLayers; i++) {
    
    int min = 1+arc4random()%3;
    int max = min+arc4random()%5;
    
    if(i==0)
    {
      unitH+=currentPosY;
      currentPosY = 0.0f;
    }
    else if(i==1)
    {
      unitH = (containerH / _numLayers);
      currentPosY = _customOffset+unitH;
    }
  
    BackgroundLayer *backgrndLayer = BackgroundLayer::createWithScrollAndGameObjects(1,containerW,unitH,kTagBGLayerInit+i,_numLayers);
    backgrndLayer->setPosition(Point(0.0f,currentPosY));
    _layers.push_back(backgrndLayer);
    bgSprite->addChild(backgrndLayer,5);
    backgrndLayer->generateRandFreq(min,max);
    
    currentPosY+=unitH;
  }
}

void GameScene::update(float dt)
{
  m_time+=1;
  if (m_time % kSpeedUpdatetime == 0) {
      updateParallaxSpeed();
  }
  if (m_time % kUpdateFrq == 0) {
      updateFrequencies();
  }
  _timer_label->setString(GameUtils::formatedTime(m_time).c_str());
}

void GameScene::updateParallaxSpeed()
{
  for(auto layer : _layers)
  {
    layer->updateParallaxSpeed();
  }
}

void GameScene::updateFrequencies()
{
  for(auto layer : _layers)
  {
    int min = 1+arc4random()%3;
    int max = min+arc4random()%6;
    layer->generateRandFreq(min, max);
  }
}

void GameScene::onExit()
{
  UserDefault::getInstance()->setIntegerForKey(kTagGameTime, m_time);
  UserDefault::getInstance()->flush();
  Layer::onExit();
}
