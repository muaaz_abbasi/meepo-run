#ifndef __GAME_SCENE_H__
#define __GAME_SCENE_H__

#include "cocos2d.h"
#include "extensions/cocos-ext.h"
#include "cocosbuilder/CocosBuilder.h"
#include "BackgroudLayer.h"

using namespace cocos2d;
using namespace cocosbuilder;

USING_NS_CC;

class GameScene : public Layer
{
public:
  static Scene* createSceneWithLayers(int pLayers,bool isFirst);
  static GameScene* createWithLayers(int pLayers,bool isFirst);
  static GameScene* instance();
  bool initWithLayers(int pLayers,bool isFirst);
  void update(float dt);
  void addHud();
  //void pauseAction();
  void updateParallaxSpeed();
  void initBackgroundLayer();
  void firstRunActions(int pLayer);
  void updateFrequencies();
  virtual void onExit();
private:
  int m_time;
  float _customOffset;
  Label* _timer_label;
  Node* _node;
  std::vector<BackgroundLayer*> _layers;
  CC_SYNTHESIZE(int, _numLayers, NumLayers);
};

#endif // __GAME_SCENE_H__
