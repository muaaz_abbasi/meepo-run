#ifndef NOBDYDIESALN_GameScrollingLayer__
#define NOBDYDIESALN_GameScrollingLayer__

#include <iostream>
#include "cocos2d.h"
using namespace cocos2d;
using namespace std;

class GameScrollingLayer : public Layer
{
  vector<Sprite*> spritesList_;
  float speed_;
  bool isRunning_;
  float totalPositionX_;
public:
  virtual bool init(const vector<string>& spriteNames, float sp);
  void updateSpeed(float newSpeed);
  void startScrolling();
  void stopScrolling();
  void move(float dt);
  float getSpeed();
  
};

#endif /* defined(__GameScrollingLayer__) */
