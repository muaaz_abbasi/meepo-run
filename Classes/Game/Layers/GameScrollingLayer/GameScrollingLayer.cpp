#include "GameScrollingLayer.h"

bool GameScrollingLayer:: init(const vector<string>& spriteNames, float sp)
{
  if ( !CCLayer::init() )
  {
    return false;
  }
  speed_ = sp;
  totalPositionX_ = 0;
  for(vector<string>::size_type i = 0; i != spriteNames.size(); i++)
  {
    string name = spriteNames[i];
    Sprite* sprite = Sprite::createWithSpriteFrameName(name.c_str());
    spritesList_.push_back(sprite);
    sprite->setAnchorPoint(Point(0,0));
    this->addChild(sprite);
    sprite->setPosition(Point(totalPositionX_, 0));
    totalPositionX_ += sprite->getContentSize().width;
  }
  return true;
}

float GameScrollingLayer::getSpeed()
{
  return speed_;
}

void GameScrollingLayer::updateSpeed(float newSpeed)
{
  speed_ = newSpeed;
}

void GameScrollingLayer:: startScrolling()
{
  isRunning_ = true;
  this->schedule(schedule_selector(GameScrollingLayer::move),0.005);
}

void GameScrollingLayer:: move(float dt)
{
  float s = 50*speed_*dt;
  Sprite* sp= NULL;
  for(vector<Sprite>::size_type i = 0; i != spritesList_.size(); i++)
  {
    sp = spritesList_[i];
    float positionX = sp->getPosition().x;
    positionX-=s;
    float spriteWidth = sp->getContentSize().width;
    if(positionX<(spriteWidth*-1))
    {
      float positionDelta = (spriteWidth*-1) - positionX;
      positionX  = totalPositionX_-spriteWidth-positionDelta;
    }
    sp->setPosition(Point(positionX, 0));
  }
}

void GameScrollingLayer:: stopScrolling()
{
  this->unschedule(schedule_selector(GameScrollingLayer::move));
  isRunning_ =false;
}

