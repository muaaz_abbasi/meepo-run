#include "PauseLayer.h"
#include "GameUtils.h"
#include "GameConstants.h"
#include "MainMenu.h"

#define ShopItemPng1 "inner_panel.png"
void PauseLayer:: onEnter()
{
  Layer::onEnter();
}

PauseLayer* PauseLayer::create()
{
  PauseLayer *pLayer = new PauseLayer();
  if (pLayer && pLayer->init())
  {
    pLayer->autorelease();
    pLayer->swallowTouches();
    return pLayer;
  }
  CC_SAFE_DELETE(pLayer);
  return NULL;
}

bool PauseLayer::init()
{
  if ( !Layer::init() )
  {
      return false;
  }
  
  NodeLoaderLibrary* ccNodeLoaderLibrary = NodeLoaderLibrary::newDefaultNodeLoaderLibrary();
  CCBReader* ccbReader = new CCBReader(ccNodeLoaderLibrary);
  ccbReader->autorelease();
  
  _node = ccbReader->readNodeGraphFromFile("PauseLayer.ccbi", this, TARGET_DESIGN_RESOLUTION_SIZE);
  if(_node)
      this->addChild(_node);
  GameUtils::adjustScreen(_node);
  
  return true;
}

SEL_MenuHandler PauseLayer::onResolveCCBCCMenuItemSelector(Ref * pTarget, const char * pSelectorName)
{
  CCB_SELECTORRESOLVER_CCMENUITEM_GLUE(this, pSelectorName, PauseLayer::onMenuItemPressed);
  return NULL;
}

extension::Control::Handler PauseLayer::onResolveCCBCCControlSelector(Ref * pTarget, const char * pSelectorName)
{
  return NULL;
}
bool PauseLayer::onAssignCCBMemberVariable(Ref * pTarget, const char * pMemberVariableName, Node * node)
{
  return false;
}

void PauseLayer::onMenuItemPressed(Ref *obj)
{
  MenuItem *menuItem = (MenuItem *) obj;
  int tag = menuItem->getTag();
  
  if(tag == kTagPLResume)
  {
    removeFromParentAndCleanup(true);
  }
  if(tag == kTagPLMainMenu)
  {
    auto scene = MainMenu::createScene();
    Director::getInstance()->replaceScene(scene);
  }
}

void PauseLayer::swallowTouches()
{
  auto listener = EventListenerTouchOneByOne::create();
  listener->setSwallowTouches(true);
  listener->onTouchBegan=CC_CALLBACK_2(PauseLayer::onTouchBegan, this);
  _eventDispatcher->addEventListenerWithSceneGraphPriority(listener, this);
}

bool PauseLayer::onTouchBegan(Touch* touch, Event* event)
{
    return true;
}

void PauseLayer:: onExit()
{
  Director::getInstance()->resume();
  Layer::onExit();
}



