#ifndef __Pause_Layer_H__
#define __Pause_Layer_H__

#include "cocos2d.h"
#include "extensions/cocos-ext.h"
#include "cocosbuilder/CocosBuilder.h"

using namespace cocos2d;
using namespace cocosbuilder;
using namespace cocos2d::extension;

class PauseLayer: public Layer, public CCBMemberVariableAssigner, public CCBSelectorResolver
{
public:
  
  static PauseLayer* create();
  bool init();
  virtual SEL_MenuHandler onResolveCCBCCMenuItemSelector(Ref * pTarget, const char * pSelectorName);
  virtual extension::Control::Handler onResolveCCBCCControlSelector(Ref * pTarget, const char * pSelectorName);
  virtual bool onAssignCCBMemberVariable(Ref * pTarget, const char * pMemberVariableName, Node * node);
  void swallowTouches();
  virtual bool onTouchBegan(Touch* touch, Event* event);
  void onMenuItemPressed(Ref* obj);
  virtual void onExit();
  virtual void onEnter();
private:
  EventListenerTouchOneByOne* _listener;
  Node* _node;
};

#endif // __Pause_Layer_H__
