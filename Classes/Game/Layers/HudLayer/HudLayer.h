#ifndef __Hud_Layer_H__
#define __Hud_Layer_H__

#include "cocos2d.h"
#include "extensions/cocos-ext.h"
#include "cocosbuilder/CocosBuilder.h"

using namespace cocos2d;
using namespace cocosbuilder;
using namespace cocos2d::extension;

class HudLayer: public Layer, public CCBMemberVariableAssigner, public CCBSelectorResolver
{
public:
  static HudLayer* create();
  bool init();
  virtual SEL_MenuHandler onResolveCCBCCMenuItemSelector(Ref * pTarget, const char * pSelectorName);
  virtual extension::Control::Handler onResolveCCBCCControlSelector(Ref * pTarget, const char * pSelectorName);
  virtual bool onAssignCCBMemberVariable(Ref * pTarget, const char * pMemberVariableName, Node * node);
  void onMenuItemPressed(Ref* obj);
  virtual void onExit();
  virtual void onEnter();
private:
  Node* _hud;
};

#endif // __Hud_Layer_H__
