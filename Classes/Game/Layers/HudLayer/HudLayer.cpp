#include "HudLayer.h"
#include "GameUtils.h"
#include "PauseLayer.h"
#include "GameConstants.h"

void HudLayer:: onEnter()
{
  Layer::onEnter();
}

HudLayer* HudLayer::create()
{
  HudLayer *pLayer = new HudLayer();
  if (pLayer && pLayer->init())
  {
    pLayer->autorelease();
    return pLayer;
  }
  CC_SAFE_DELETE(pLayer);
  return NULL;
}

bool HudLayer::init()
{
  if ( !Layer::init() )
  {
      return false;
  }
  
  NodeLoaderLibrary* ccNodeLoaderLibrary = NodeLoaderLibrary::newDefaultNodeLoaderLibrary();
  CCBReader* ccbReader = new CCBReader(ccNodeLoaderLibrary);
  ccbReader->autorelease();
  _hud = ccbReader->readNodeGraphFromFile("Hud.ccbi", this, TARGET_DESIGN_RESOLUTION_SIZE);
  _hud->setAnchorPoint(Point::ZERO);
  _hud->setTag(1010);
  if(_hud)
    this->addChild(_hud);
    //GameUtils::adjustScreen(_hud);
  
  return true;
}

SEL_MenuHandler HudLayer::onResolveCCBCCMenuItemSelector(Ref * pTarget, const char * pSelectorName)
{
  CCB_SELECTORRESOLVER_CCMENUITEM_GLUE(this, pSelectorName, HudLayer::onMenuItemPressed);
  return NULL;
}

extension::Control::Handler HudLayer::onResolveCCBCCControlSelector(Ref * pTarget, const char * pSelectorName)
{
  return NULL;
}
bool HudLayer::onAssignCCBMemberVariable(Ref * pTarget, const char * pMemberVariableName, Node * node)
{
  return false;
}

void HudLayer::onMenuItemPressed(Ref *obj)
{
  MenuItem *menuItem = (MenuItem *) obj;
  if(menuItem->getTag() == 101)
  {
    Director::getInstance()->pause();
    PauseLayer *Pauselayer = (PauseLayer*)PauseLayer::create();
    addChild(Pauselayer);
  }
}

void HudLayer:: onExit()
{
  Director::getInstance()->resume();
  Layer::onExit();
}



