#ifndef __BACKGROUND_LAYER_H__
#define __BACKGROUND_LAYER_H__

#include "cocos2d.h"
#include "GameScrollingLayer.h"
using namespace cocos2d;
using namespace std;

class BackgroundLayer : public Layer
{
public:
  bool initWithGameInfo(int pHero,float width,float height,int tag,int layers);
  void addScrollAndGameObjects();
  void addHero();
  void addHurdle(float dt);
  void heroJump();
  void eraseNode(Node* node);
  void enableJump();
  void gameOverPre();
  void menuCloseCallback(cocos2d::Ref* pSender);
  static BackgroundLayer* createWithScrollAndGameObjects(int pHero,float width,float height,int tag,int layers);
  void update(float dt);
  void addTouchEvents();
  void removeTouchEvents();
  virtual bool onTouchBegan(Touch* touch, Event* event);
  bool isCollisionBetweenSpriteA(Sprite* spr1, Sprite* spr2, bool pp);
  void updateParallaxSpeed();
  void parallaxScroll(float dt);
  void generateHurdles();
  void generateRandFreq(int min ,int max);
  virtual void onExit();
private:
  GameScrollingLayer* layer_;
  Sprite* _hero;
  vector<string> _scrollImages;
  vector<Node*> _hurdles;
  bool _heroJumping;
  float _width;
  float _height;
  float _customOffset;
  float _offsetY;
  RenderTexture* _rt;
  SpriteBatchNode* _spritesbatch;
  SpriteFrameCache* _cache;
  EventListenerTouchOneByOne* _listener;
  
  CC_SYNTHESIZE(float, _jumpVelocity, JumpVelocity);
  CC_SYNTHESIZE(float, _velocity, Velocity);
  CC_SYNTHESIZE(float, _speed, Speed);
  CC_SYNTHESIZE(float, _hurdleFreq, HurdleFreq);
  CC_SYNTHESIZE(int, _heroID, HeroID);
  
};

#endif // __BACKGROUND_LAYER_H__
