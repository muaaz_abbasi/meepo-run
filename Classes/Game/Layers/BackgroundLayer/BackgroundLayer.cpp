#include "BackgroudLayer.h"
#include "GameConstants.h"
#include "GameOverPre.h"
#include "GameUtils.h"
#include "GameScene.h"

USING_NS_CC;
BackgroundLayer* BackgroundLayer::createWithScrollAndGameObjects(int pHero,float width,float height,int tag,int layers)
{
  BackgroundLayer *pLayer = new BackgroundLayer();
  if (pLayer && pLayer->initWithGameInfo(pHero,width,height,tag,layers))
  {
    pLayer->autorelease();
    pLayer->addTouchEvents();
    return pLayer;
  }
  CC_SAFE_DELETE(pLayer);
  return NULL;
}

bool BackgroundLayer::initWithGameInfo(int pHero,float width,float height,int tag,int layers)
{
  if ( !Layer::init() )
  {
      return false;
  }
  setHeroID(pHero);
  setTag(tag);
  setSpeed(KInitialSpeed);
  setVelocity(KInitialVelocity);
  setJumpVelocity(KInitialJumpVelocity);
  
  _spritesbatch = SpriteBatchNode::create("GamePlay.pvr.ccz");
  addChild(_spritesbatch);
  _cache = SpriteFrameCache::getInstance();
  _cache->addSpriteFramesWithFile("GamePlay.plist");

  _width =width;
  _height=height;
  _customOffset = GameUtils::getCustomOffset(layers);
  setContentSize(Size(width,height));
    
  _heroJumping = false;
  _rt = CCRenderTexture::create(_width, _height);
  _rt->setVisible(false);
  addChild(_rt);
  addScrollAndGameObjects();
  
  return true;
}

void BackgroundLayer::generateHurdles()
{
  schedule(schedule_selector(BackgroundLayer::addHurdle), getHurdleFreq());
}

void BackgroundLayer::generateRandFreq(int min , int max)
{
  int randomFreq = GameUtils::getUnique(min, max);
  setHurdleFreq(randomFreq);
  generateHurdles();
  unscheduleUpdate();
  scheduleUpdate();
}

void BackgroundLayer::update(float dt)
{
  for (int i=0;i!=_hurdles.size(); i++) {
    Sprite* hurdle = (Sprite*)_hurdles[i];
    if(isCollisionBetweenSpriteA(_hero, hurdle, true))
    {
      gameOverPre();
    }
  }
}

void BackgroundLayer::gameOverPre()
{
  GameOverPre *preOver = (GameOverPre*)GameOverPre::createScene();
  preOver->setTag(kTagGameScenePopup);
  Director::getInstance()->pushScene((Scene*)preOver);
}

bool BackgroundLayer::isCollisionBetweenSpriteA(Sprite* spr1, Sprite* spr2, bool pp)
{
  bool isCollision = false;
	Rect intersection=Rect::ZERO;
	Rect r1 = spr1->boundingBox();
	Rect r2 = spr2->boundingBox();
  intersection.setRect(max(r1.getMinX(),r2.getMinX()), max(r1.getMinY(),r2.getMinY()),0,0);
  intersection.size.width = min(r1.getMidX(), r2.getMaxX()) - intersection.getMinX();
  intersection.size.height = min(r1.getMaxY(), r2.getMaxY()) - intersection.getMinY();
	if ( (intersection.size.width>0) && (intersection.size.height>0) )
  {
    if (!pp) {
      return true;
    }
    unsigned int x = intersection.origin.x;
    unsigned int y = intersection.origin.y;
    unsigned int w = intersection.size.width;
    unsigned int h = intersection.size.height;
    unsigned int numPixels = w * h;
    _rt->beginWithClear( 0, 0, 0, 0);
    
    // Get color values of intersection area
    Color4B *buffer = (Color4B *)malloc( sizeof(Color4B) * numPixels );
    glReadPixels(x, y, w, h, GL_RGBA, GL_UNSIGNED_BYTE, buffer);
    
    _rt->end();
    
    // Read buffer
    unsigned int step = 1;
    for(unsigned int i=0; i<numPixels; i+=step)
    {
      Color4B color = buffer[i];
      if (color.r > 0 && color.g > 0)
      {
        isCollision = true;
        break;
      }
    }
    
    // Free buffer memory
    free(buffer);
  }
  return isCollision;

}

void BackgroundLayer::updateParallaxSpeed()
{
  float currSpeed = getSpeed();
  float velocity = getVelocity();
  setSpeed(currSpeed+kSpeedInc);
  layer_->updateSpeed(getSpeed());
  setVelocity(velocity+kVInc);
  
  if (getJumpVelocity()<20)
  {
    setJumpVelocity(getJumpVelocity()+kJumpVInc1);
    generateRandFreq(1, 4);
  }
  else
  {
    setJumpVelocity(getJumpVelocity()+kJumpVInc2);
    generateRandFreq(1, 3);
  }
}

void BackgroundLayer::addScrollAndGameObjects()
{
  _scrollImages.push_back("land.png");
  _scrollImages.push_back("land.png");
 
  layer_ = new GameScrollingLayer();
  layer_->init(_scrollImages, getSpeed());
  addChild(layer_,kBGLayerParallaxZorder);
  layer_->setPosition(Point(0,0));
  
  if (getTag()==kTagBGLayerInit) {
    layer_->setPosition(Point(0,_customOffset));
  }
  
  layer_->startScrolling();
  addHero();
  generateRandFreq(1,1);
}

void BackgroundLayer::addHero()
{
  float offsetY = kOffsetHeroEtc;
  _hero = Sprite::createWithSpriteFrameName("runner0.png");
  _hero->setTag(kTagBGLayerHero);
  
  if(getTag()==kTagBGLayerInit)
    offsetY+=_customOffset;

  _hero->setPosition(Point(_width/2-kOffsetXHeroEtc,offsetY));
  _spritesbatch->addChild(_hero);
  
  Vector<SpriteFrame*> animFrames;
  char str[100] = {0};
  for(int i = 0; i < 7; i++)
  {
    sprintf(str, "runner%d.png", i);
    SpriteFrame* frame = _cache->getSpriteFrameByName( str );
    animFrames.pushBack(frame);
  }
  
  Animation* animation = Animation::createWithSpriteFrames(animFrames, .04f);
  _hero->runAction( RepeatForever::create( Animate::create(animation)));
}

void BackgroundLayer::addHurdle(float dt)
{
  float offsetY = kOffsetHeroEtc;
  int diff = 5;
  int rand = 1+random()%2;
  char str[100] = {0};
  sprintf(str, "hurdle%d.png", rand);
  
  if(getTag()==kTagBGLayerInit)
    offsetY+=_customOffset;
  
  Sprite* hurdle = Sprite::createWithSpriteFrameName(str);
  //  Hurdle *hurdle = (Hurdle*)Hurdle::instance()->initWithFile(str, getVelocity());
 // hurdle->setVelocity1(getVelocity());
    
  if(rand==1){
    hurdle->setTag(kTagBGLayerHurdle2);
    diff = -25;
  }else
    hurdle->setTag(kTagBGLayerHurdle1);

  hurdle->setPosition(Point(_width+10,offsetY+diff));
  this->addChild(hurdle,20);
    
   
    //hurdle->move();
    
  _hurdles.push_back(hurdle);
  
  Point begin = hurdle->getPosition();
  Point end = Point(-hurdle->getContentSize().width, offsetY+diff);
  float time = begin.getDistance(end)/getVelocity();

  auto move = MoveTo::create(time, Point(-hurdle->getContentSize().width,offsetY+diff));
  hurdle->runAction
  (
   Sequence::create
   (move,
    CallFuncN::create(CC_CALLBACK_1(BackgroundLayer::eraseNode,this)),
    NULL)
  );
  
}

void BackgroundLayer::eraseNode(Node* node)
{
  _hurdles.erase(std::remove(_hurdles.begin(), _hurdles.end(), node), _hurdles.end());
  node->removeFromParentAndCleanup(true);
}

void BackgroundLayer::addTouchEvents()
{
  _listener = EventListenerTouchOneByOne::create();
  _listener->onTouchBegan = CC_CALLBACK_2(BackgroundLayer::onTouchBegan, this);
  Director::getInstance()->getEventDispatcher()->addEventListenerWithFixedPriority(_listener, 30);
}

bool BackgroundLayer::onTouchBegan(Touch *touch, Event *event)
{
  Point touchBegin = Director::getInstance()->convertToGL(touch->getLocationInView());
  Rect rect = this->getBoundingBox();
  if(rect.containsPoint(touchBegin))
  {
    CCLOG("Muaaz::Touch imapct%d",this->getTag());
    if(!_heroJumping)
    {
      _heroJumping=true;
      heroJump();
    }
  }
  return true;
}

void BackgroundLayer::removeTouchEvents()
{
  getEventDispatcher()->removeEventListener(_listener);
}

void BackgroundLayer::heroJump()
{
  Node* node = NULL;
  float h = 90;
  float sJump = getSpeed()/getJumpVelocity();
  if(_hurdles.size()!=0)
  {
    node = _hurdles.front();
    if(node->getTag()==kTagBGLayerHurdle1)
      h = node->getContentSize().height;
  }
  auto jump = JumpTo::create(sJump, _hero->getPosition(), h+kHeroSafeJumpHeight, 1);
  _hero->runAction
  (
    Sequence::create(
     jump,
     CallFunc::create(CC_CALLBACK_0(BackgroundLayer::enableJump,this)),
     NULL
    )
  );
}

void BackgroundLayer::enableJump()
{
  _heroJumping=false;
}

void BackgroundLayer:: onExit()
{
  removeTouchEvents();
  Layer::onExit();
}

void BackgroundLayer::menuCloseCallback(Ref* pSender)
{
  Director::getInstance()->end();
  exit(0);
}
