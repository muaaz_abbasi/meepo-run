
#include "ShopItem.h"
#include "InAppManager.h"

#define ShopItemPng1 "inner_panel.png"
#define ShopItemPng2 "inner_panel_labeled.png"

void ShopItem:: onEnter()
{
  CCNode::onEnter();
}

ShopItem* ShopItem::createWithSpecs(int type,string identifier,float price, int quantity)
{
  ShopItem *pLayer = new ShopItem();
  if (pLayer && pLayer->initItem(type,identifier,price,quantity))
  {
    pLayer->autorelease();
    return pLayer;
  }
  CC_SAFE_DELETE(pLayer);
  return NULL;
}


bool ShopItem:: initItem(int type,string identifier,float price, int quantity)
{
  if (!Node::init())
  {
    return false;
  }
  
  Menu* menu=NULL;
  MenuItem *item=NULL;
  _identifier = identifier;

  if(type == kShopItemNonAd){
    itemDisplay = Sprite::createWithSpriteFrameName(ShopItemPng1);
    item = MenuItemSprite::create(itemDisplay,itemDisplay,CC_CALLBACK_0(ShopItem::doPurchase,this));
  }else{
    itemDisplay = Sprite::createWithSpriteFrameName(ShopItemPng2);
    item = MenuItemSprite::create(itemDisplay,itemDisplay,CC_CALLBACK_0(ShopItem::doPurchase,this));
  }
  
  Label *itemsLbl = Label::createWithTTF(String::createWithFormat("+%d",quantity)->getCString(), GameFonts, 25);
  itemsLbl->setPosition(Point(itemDisplay->getContentSize().width/2,itemDisplay->getContentSize().height/2+10));
  itemDisplay->addChild(itemsLbl);
  
    //add price
  Label *priceLbl = Label::createWithTTF(String::createWithFormat("%.2f",price)->getCString(), GameFonts, 18);
  priceLbl->setPosition(Point(itemDisplay->getContentSize().width/2+10,itemDisplay->getContentSize().height/2-40));
  itemDisplay->addChild(priceLbl);
  
  menu = Menu::create(item, NULL);
  menu->setPosition(Point(getContentSize().width/2,getContentSize().height/2));
  addChild(menu,100);
  
  return true;
}

void ShopItem::doPurchase()
{
  log("Muaaz+++BUY CALL++++ITEM ID = %s",_identifier.c_str());
  InAppManager::sharedHandler()->purchaseObjectWithID(_identifier.c_str());

}

void ShopItem:: onExit()
{
  Node::onExit();
}
