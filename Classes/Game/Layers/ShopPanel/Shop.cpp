#include "Shop.h"
#include "GameUtils.h"
#include "ShopItem.h"
#include "GameOverPre.h"

#define ShopItemPng1 "inner_panel.png"
void Shop:: onEnter()
{
  Layer::onEnter();
}

Shop* Shop::create(int lives)
{
  Shop *pLayer = new Shop();
  if (pLayer && pLayer->init(lives))
  {
    pLayer->autorelease();
    pLayer->swallowTouches();
    return pLayer;
  }
  CC_SAFE_DELETE(pLayer);
  return NULL;
}

bool Shop::init(int lives)
{
  if ( !Layer::init() )
  {
      return false;
  }
  
  _lives = lives;
  NodeLoaderLibrary* ccNodeLoaderLibrary = NodeLoaderLibrary::newDefaultNodeLoaderLibrary();
  CCBReader* ccbReader = new CCBReader(ccNodeLoaderLibrary);
  ccbReader->autorelease();
  
  _node = ccbReader->readNodeGraphFromFile("ShopPanel.ccbi", this, TARGET_DESIGN_RESOLUTION_SIZE);
  if(_node)
      this->addChild(_node);
  GameUtils::adjustScreen(_node);
  Label* livesCurrentLbl = (Label*)_node->getChildByTag(kTagLivesCurrentLblShop);
  livesCurrentLbl->setString(String::createWithFormat("%d",_lives)->getCString());
  
  schedule(schedule_selector(Shop::synchLives));
  //setupShopView();
  return true;
}

void Shop::setupShopView()
{
  auto bg = (Sprite*)_node->getChildByTag(kTagShopView);
  
  ValueMap plistMap = FileUtils::getInstance()->getValueMapFromFile(kGameConfig);
  ValueMap inappSpecs = plistMap.at(kInappSpecs).asValueMap();
  int total_inapps = inappSpecs.at("Total").asInt();
  
  auto itemBg = Sprite::createWithSpriteFrameName(ShopItemPng1);
  float unitw = itemBg->getContentSize().width;
  float unith = itemBg->getContentSize().height;
  
  Layer* container = Layer::create();
  container->setContentSize(Size(unitw*3.5,unith*2.6));
  container->setAnchorPoint(Point::ZERO);
  container->setPosition(Point(32,118));

  int num_cols = 3;
  float unitposX = 55;
  float unitposY = container->getContentSize().height/2-80;
  
  for (int i=1; i<=total_inapps; i++) {
    
    string item = String::createWithFormat("item%d",i)->getCString();
    ValueMap itemMap = inappSpecs.at(item).asValueMap();
   
    int  type  = itemMap.at("Type").asInt();
    string id  = itemMap.at("Identifier").asString();
    float cost = itemMap.at("Cost").asFloat();
    int  quant = itemMap.at("Quantity").asInt();
    
    ShopItem* shItem = ShopItem::createWithSpecs(type, id, cost, quant);
    
    shItem->setPosition(Point(unitposX,unitposY));
    container->addChild(shItem);
    
    if(i%num_cols==0){
      unitposX  =55;
      unitposY +=unith+20;
    }else{
      unitposX +=unitw+10;
    }
  }
  
  bg->addChild(container);
  
}
SEL_MenuHandler Shop::onResolveCCBCCMenuItemSelector(Ref * pTarget, const char * pSelectorName)
{
  CCB_SELECTORRESOLVER_CCMENUITEM_GLUE(this, pSelectorName, Shop::onMenuItemPressed);
  return NULL;
}

extension::Control::Handler Shop::onResolveCCBCCControlSelector(Ref * pTarget, const char * pSelectorName)
{
  return NULL;
}
bool Shop::onAssignCCBMemberVariable(Ref * pTarget, const char * pMemberVariableName, Node * node)
{
  return false;
}

void Shop::onMenuItemPressed(Ref *obj)
{
  MenuItem *menuItem = (MenuItem *) obj;
  int tag = menuItem->getTag();
  
  if(tag == kTagBackItemShop)
    removeFromParentAndCleanup(true);
  
}

void Shop::swallowTouches()
{
  auto listener = EventListenerTouchOneByOne::create();
  listener->setSwallowTouches(true);
  listener->onTouchBegan=CC_CALLBACK_2(Shop::onTouchBegan, this);
  _eventDispatcher->addEventListenerWithSceneGraphPriority(listener, this);
}

void Shop::synchLives(float dt)
{
  int lives = UserDefault::getInstance()->getIntegerForKey(kPlayerLives);
  Label* livesCurrentLbl = (Label*)_node->getChildByTag(kTagLivesCurrentLblShop);
  livesCurrentLbl->setString(String::createWithFormat("%d",lives)->getCString());
}

bool Shop::onTouchBegan(Touch* touch, Event* event)
{
    return true;
}

void Shop:: onExit()
{
  Layer::onExit();
  GameOverPre* parent = (GameOverPre*)getParent();
  parent->onCloseShop();
}



