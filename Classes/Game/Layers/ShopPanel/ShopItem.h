
#ifndef ShopItem_h
#define ShopItem_h

#include "cocos2d.h"
#include "GameUtils.h"
#include "GameConstants.h"

using namespace cocos2d;

class ShopItem : public Node
{
public:
  static ShopItem* createWithSpecs(int type,string identifier,float price, int quantity);
  virtual bool initItem(int type,string identifier,float price, int quantity);
  void doPurchase();
  virtual void onEnter();
  virtual void onExit();
private:
  string _identifier;
  Point _touchBegin;
  Sprite* itemDisplay;
};

#endif
