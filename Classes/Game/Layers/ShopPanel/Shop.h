#ifndef __SHOP_SCENE_H__
#define __ShOP_SCENE_H__

#include "cocos2d.h"
#include "extensions/cocos-ext.h"
#include "cocosbuilder/CocosBuilder.h"
  //#include "BackgroudLayer.h"

using namespace cocos2d;
using namespace cocosbuilder;
using namespace cocos2d::extension;

class Shop: public Layer, public CCBMemberVariableAssigner, public CCBSelectorResolver
{
public:
  
  static Shop* create(int lives);
  bool init(int lives);
  virtual SEL_MenuHandler onResolveCCBCCMenuItemSelector(Ref * pTarget, const char * pSelectorName);
  virtual extension::Control::Handler onResolveCCBCCControlSelector(Ref * pTarget, const char * pSelectorName);
  virtual bool onAssignCCBMemberVariable(Ref * pTarget, const char * pMemberVariableName, Node * node);
  void swallowTouches();
  void doPurchase();
  virtual bool onTouchBegan(Touch* touch, Event* event);
  void setupShopView();
  void onMenuItemPressed(Ref* obj);
  void synchLives(float dt);
  virtual void onExit();
  virtual void onEnter();
private:
  EventListenerTouchOneByOne* _listener;
  Node* _node;
  int _lives;
};

#endif // __GAMEOVER_SCENE_H__
