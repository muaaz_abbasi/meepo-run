typedef struct tagResource
{
  cocos2d::Size size;
  char directory[100];
}Resource;

static Resource resource1  =  { cocos2d::Size(320, 480),   "iphone" };
static Resource resource2  =  { cocos2d::Size(640,960), "ipad" };
static Resource resource3  =  { cocos2d::Size(1280,1920), "ipadhd" };
static cocos2d::Size TARGET_DESIGN_RESOLUTION_SIZE = cocos2d::Size(640,960);

//GameScene Tags
#define kTagGameSceneBG 0
#define kSpeedUpdatetime 600
#define kUpdateFrq 15
#define kTagGameScenePopup 15
#define kTimeCurrent "currentTime"

#define kTagHudBg 0

//Gamebackground layer
#define kTagBGLayerHero 20
#define kTagBGLayerHurdle1 25
#define kTagBGLayerHurdle2 40
#define kTagBGLayerInit 500
#define kTagTimerLabel 30
#define KInitialSpeed 10.0
#define KInitialVelocity 500
#define KInitialJumpVelocity 15.0
#define kHurdleDiff 5
#define kBGLayerParallaxZorder 5
#define kOffsetXHeroEtc 250
#define kOffsetHeroEtc 65
#define kHeroSafeJumpHeight 35
#define kSpeedInc 1
#define kVInc 50
#define kJumpVInc1 1.0
#define kJumpVInc2 5.0

//MainMenu constants
#define kSoundEnabled "isSoundEnabled"
#define kMusicEnabled "isMusicEnabled"
#define kMoreGamesUrl "www.google.com"
#define kGameData "GameData"
#define kGameModes "GameModes"
#define kTagMenu 100
#define kTagSoundItem 107
#define kTagMusicItem 106

enum menuItemsTags{
  kTagNormalMode=101, 
  kTagHardMode, 
  kTagCrazyMode,
  kTagInsaneMode,
  kTagMoreGames,
  kTagMusicToggle,
  kTagSoundToggle,
  kTagRate
};

//Game constants
#define kNotSetFirst "firstTimeSet"
#define kPlayerLives "playerLives"
#define kPlayerLivesReq "playerLivesReq"
#define kPlayerLivesMax 3
#define kPlayerLivesReqMax 2
#define GameFonts "Complete_In_Him.ttf"
#define kTagGameNumLayers "num_layers"
#define kTagGameTime "time_value"
#define kTagGameTimeLast "time_Last"

//GameOver constants
#define kTagCurrntTimeLbl 10
#define kTagBestTimeLbl 12
#define kTagBackItem 301
#define kTagRestartItem 302

//GameOverPre constants
#define kTagContinue 201
#define kTagLivesCurrentLbl 202
#define kTagLivesReqLbl 203
#define kTagSkip 206

  //Shop constant
#define kGameConfig "GameConfig.plist"
#define kInappSpecs "InappSpecs"
#define kTagBackItemShop 101
#define kTagLivesCurrentLblShop 200
#define kTagShopView 1
#define kShopItemNonAd 1
#define kTagShopContainer 1000
#define IN_APP_PURCHASE "IN_APP_PURCHASE"
#define REMOVE_AD "RemoveAds"

#define MEEPO_LIVESA "com.gaminations.meepoRun.a"
#define MEEPO_LIVESB "com.gaminations.meepoRun.b"
#define MEEPO_LIVESC "com.gaminations.meepoRun.c"
#define MEEPO_LIVESD "com.gaminations.meepoRun.d"
#define MEEPO_LIVESE "com.gaminations.meepoRun.e"
#define MEEPO_LIVESF "com.gaminations.meepoRun.f"

  //PauseLayer constants
#define kTagPLmenu 100
#define kTagPLResume 101
#define kTagPLMainMenu 102
