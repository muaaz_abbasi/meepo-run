
#include "SoundHandler.h"
#include "GameConstants.h"

SoundHandler* SoundHandler::getInstance() {
  static SoundHandler *soundHandler;
  
  if(!soundHandler) {
    soundHandler = new SoundHandler();
  }
  return soundHandler;
}

void SoundHandler::initiate()
{
  if (UserDefault::getInstance()->getBoolForKey(kSoundEnabled)==false)
  {
    gameSoundOFF();
  }
  else
  {
    gameSoundON();
  }
 
  if (UserDefault::getInstance()->getBoolForKey(kMusicEnabled)==false)
  {
    gameBackGroundMusicOFF();
  }
  else
  {
    gameBackGroundMusicON();
  }
}

void SoundHandler::gameSoundON()
{
  CCLOG("GAME SOUND ON++++++");
  CocosDenshion::SimpleAudioEngine::getInstance()->setBackgroundMusicVolume(1.0f);
  CocosDenshion::SimpleAudioEngine::getInstance()->setEffectsVolume(1.0f);
}

void SoundHandler::gameSoundOFF()
{
  CCLOG("GAME SOUND OFF++++++");
  CocosDenshion:: SimpleAudioEngine::getInstance()->setBackgroundMusicVolume(0.0f);
  CocosDenshion:: SimpleAudioEngine::getInstance()->setEffectsVolume(0.0f);
}
 
void SoundHandler::gameBackGroundMusicON()
{
  CCLOG("GAME MUSIC ON++++++");
  CocosDenshion:: SimpleAudioEngine::getInstance()->setBackgroundMusicVolume(1.0f);
}

void SoundHandler::gameBackGroundMusicOFF()
{
  CCLOG("GAME MUSIC OFF++++++");
  CocosDenshion:: SimpleAudioEngine::getInstance()->stopBackgroundMusic(true);
  CocosDenshion:: SimpleAudioEngine::getInstance()->setBackgroundMusicVolume(0.0f);
}