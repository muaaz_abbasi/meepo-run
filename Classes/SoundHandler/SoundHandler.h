
#ifndef NOBDYDIESALN_SOUNDHANDLER__
#define NOBDYDIESALN_SOUNDHANDLER__

#include <iostream>
#include "cocos2d.h"
#include "SimpleAudioEngine.h"
using namespace cocos2d;

class SoundHandler
{
public:
  static SoundHandler* getInstance();
  void initiate();
  void gameSoundOFF();
  void gameSoundON();
  void gameBackGroundMusicOFF();
  void gameBackGroundMusicON();
  
};

#endif /* defined(NOBDYDIESALN_SOUNDHANDLER__) */
